//
// Shinobi - OpenLPR Plugin
// Copyright (C) 2016-2025 Moe Alam
//
// Base Init >>
const fs = require('fs');
const config = require('./conf.json')
const openLPR = require('node-openlpr')
const apiHost = config.apiHost
const {
    detectPlate,
    apiRequest,
    uploadFile,
} = openLPR(apiHost);
var s
const {
		workerData
	} = require('worker_threads');

if(workerData && workerData.ok === true){
	try{
		s = require('../pluginWorkerBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			s = require('./pluginWorkerBase.js')(__dirname,config)
		}catch(err){
			console.log(err)
			return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
		}
	}
}else{
	try{
		s = require('../pluginBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			s = require('./pluginBase.js')(__dirname,config)
		}catch(err){
			console.log(err)
			return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
		}
	}
	try{
		s = require('../pluginBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			const {
				haltMessage,
				checkStartTime,
				setStartTime,
			} = require('../pluginCheck.js')

			if(!checkStartTime()){
				console.log(haltMessage,new Date())
				s.disconnectWebSocket()
				return
			}
			setStartTime()
		}catch(err){
			console.log(`pluginCheck failed`)
		}
	}

}
// Base Init />>
function pointsToObject(points) {
  const [x1, y1, x2, y2] = points;
  return {
    x: x1,
    y: y1,
    width: x2 - x1,
    height: y2 - y1
  };
}
s.detectObject = async function(frameBuffer,d,tx){
	const timeStart = new Date()
	const result = await detectPlate(frameBuffer)
	const predictions = result.detections.boxes
	const scores = result.scores.boxes
	if(predictions.length > 0) {
		const mats = []
		predictions.forEach(function(box,n){
            const ocr = result.ocr[n] ? result.ocr[n] : result.ocr[0] || ['Unknown Plate', 0]
            const score = score[n] ? score[n] : score[0] || 0
			mats.push(Object.assign(pointsToObject(box),{
                confidence: score
				tag: ocr[0] : 'Unknown Plate',
			}))
		})
		const isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1'
		const width = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
		const height = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)
		tx({
			f:'trigger',
			id:d.id,
			ke:d.ke,
			details:{
				plug: config.plug,
				name: `OpenLPR`,
				reason: 'object',
				matrices: mats,
				imgHeight: width,
				imgWidth: height,
			},
			frame: frameBuffer
		})
	}
}
