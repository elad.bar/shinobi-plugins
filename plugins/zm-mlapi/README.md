# ZoneMinder MLAPI Plugin for Shinobi

> ZoneMinder MLAPI is a detection daemon originally designed for ZoneMinder. Due to the way it is made, it allows other applications to use it as well. So, here we are!

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **Plugin Manager** tab.
3. In the listing select **"ZoneMinder MLAPI"** plugin to Download.
4. Hit `Run Installer` for the newly downloaded plugin.
    - This installer will not install ZM MLAPI. That must already be installed by following the steps here : https://github.com/ZoneMinder/mlapi
5. Configure the plugin's conf.json to point at your ZM ML API server. Set the following parameters.
    - `zmMlApiUser` : The username required to access your ZM ML API server.
    - `zmMlApiPassword` : The password for the username.
    - `zmMlapiEndpoint` : The web address for API requests.
6. Once Installed click "Enable" and restart Shinobi.
7. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)
