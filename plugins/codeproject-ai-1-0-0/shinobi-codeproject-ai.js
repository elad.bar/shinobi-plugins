const { spawn } = require('child_process');
const { workerData } = require('worker_threads');

const fs = require('fs');
const moment = require('moment');
const request = require("request");

const pluginConfig = require('./conf.json');

const MIN_CONFIDENCE_KEY = "min_confidence"
const FACE_UNKNOWN = "unknown";
const DELIMITER = "___";
const API_KEY = "api_key";
const MAX_ATTEMPTS_LOAD_BASE = 2;

const STATUS_STARTED = "started";

const EVENT_TYPES = {
    Trigger: "trigger",
    RecompileFaceDescriptors: "recompileFaceDescriptors"
}
        
const PROTOCOLS = {
    true: "https",
    false: "http"
};

const PREDICTION_KEYS = {
    Default: "label",
    UserID: "userid"
}

const AI_SERVER_MANAGEMENT_ENDPOINTS = {
    ListStatus: "/status/analysis/list",
    Settings: "/settings"
};

const FACE_RECOGNITION_ENPOINTS = {
    Processing: "/vision/face/recognize",
    List: "/vision/face/list",
    Register: "/vision/face/register",
    Unregister: "/vision/face/delete"
}

const DEFAULT_MODULES = {
    "SceneClassification": "/vision/scene",
    "ObjectDetectionYolo": "/vision/detection",
    "ObjectDetectionNet": "/vision/detection",
};

const MODULES_REASON = {
    "SceneClassification": "scene",
    "ObjectDetectionYolo": "object",
    "ObjectDetectionNet": "object",
    "FaceProcessing": "face"
};

const DEFAULT_MODULE_INITIALIZER = (moduleDetails, serverConfig) => new AIModule(moduleDetails, serverConfig);

const MODULE_INITIALIZERS = {
    "SceneClassification": (moduleDetails, serverConfig) => new SceneClassifierModule(moduleDetails, serverConfig),
    "FaceProcessing": (moduleDetails, serverConfig) => new FaceRecognitionModule(moduleDetails, serverConfig)
};

class BaseClass {
    className = null;

    constructor(thisName) {
        this.className = thisName;
    }

    log(logger, message) {
        logger(`${moment().format()} [CodeProjectAI::${this.className}]  ${message}`);
    }
    
    logError(message) {
        this.log(console.error, message);
    };
    
    logWarn(message) {
        this.log(console.warn, message);
    };
    
    logInfo(message) {
        this.log(console.info, message);
    };
    
    logDebug(message) {
        this.log(console.debug, message);
    };
}

class AIModule extends BaseClass{
    details = null;
    id = null;
    status = null;
    status = null;
    isActive = false;
    hasMinimumConfidence = true;
    hasSinglePrediction = false;
    processingEndpoint = null;
    predictionKey = null;
    baseUrl = null;
    apiKey = null;
    prcessHandlers = null;
    
    constructor(moduleDetails, serverConfig, processingEndpoint = null, predictionKey = PREDICTION_KEYS.Default) {
        const moduleId = moduleDetails.moduleId;

        super(`AIModule::${moduleId}`);

        this.details = moduleDetails;
        this.id = moduleId;
        this.name = moduleDetails.name;
        this.status = moduleDetails.status;

        this.isActive = this.status.toLowerCase() === STATUS_STARTED;

        this.processingEndpoint = processingEndpoint === null ? this.getProcessingEndpoint() : processingEndpoint;
        this.predictionKey = predictionKey;
    
        this.baseUrl = serverConfig.baseUrl;
        this.apiKey = serverConfig.apiKey;

        this.prcessHandlers = {};
    }

    getProcessingEndpoint() {
        const defaultModulesKeys = Object.keys(DEFAULT_MODULES);
        const processingEndpoint = defaultModulesKeys.includes(this.id) ? DEFAULT_MODULES[this.id] : `vision/custom/${this.id}`

        return processingEndpoint;
    }
    
    getCurrentTimestamp() {
        const currentTime = new Date();
        const currentTimestamp = currentTime.getTime();

        return currentTimestamp
    };

    getDuration(requestTime) {
        const currentTimestamp = this.getCurrentTimestamp();

        const duration = currentTimestamp - requestTime;

        return duration;
    };

    httpGet(endpoint, callback, args, checkErrorReason = false) {
        const url = `${this.serverConfig.baseUrl}${endpoint}`;

        const requestTime = this.getCurrentTimestamp();

        request.get(url, (err, res, body) => this.handleHttpResponse(err, res, body, requestTime, callback, args, checkErrorReason)); 
    }

    httpPost(requestData, callback, args, checkErrorReason = false) {
        const requestTime = this.getCurrentTimestamp();
    
        request.post(requestData, (err, res, body) => this.handleHttpResponse(err, res, body, requestTime, callback, args, checkErrorReason)); 
    }

    handleHttpResponse(err, _res, body, requestTime, callback, args, checkErrorReason) {
        const duration = this.getDuration(requestTime);
        const errors = [];
        const data = err ? null : JSON.parse(body);
        const success = data !== null && data.success;
        const isFaceNotFoundError = data !== null && checkErrorReason && data.error === "No face found in image";

        if(err) {
            errors.push(err);

        } else if (!success && isFaceNotFoundError) {
            data.ok = true;
            data.predictions = [];

        } else if (!success && !isFaceNotFoundError) {
            errors.push(data.error);

        }

        if(errors.length === 0) {
            if(!!callback) {
                callback(data, args, duration);
            }

        } else {
            const mainError = errors[0];

            this.logError(`Failed to POST data to ${requestData.url}, Error: ${JSON.stringify(mainError)}, Response time: ${duration} ms`);
        }
        
        if(!!args && !!args.onProcessCompleted) {
            args.onProcessCompleted();    
        }
    }

    process(d, tx, imageBytes, imagePath, onProcessCompleted) {
        try{
            const imageStream = fs.createReadStream(imagePath);

            const formData = {
                image: imageStream
            };
    
            if(this.hasMinimumConfidence) {
                formData[MIN_CONFIDENCE_KEY] = pluginConfig.api.minConfidence;
            }
    
            const requestData = this.getRequestData(this.processingEndpoint, formData);
            const args = {
                imageBytes: imageBytes,
                d: d,
                tx: tx,
                onProcessCompleted: onProcessCompleted
            }

            this.httpPost(requestData, (data, args, duration) => this.onProcessedImage(data, args, duration), args, true);
        } catch(ex) {
            this.logError(`Failed to process image, Error: ${ex}`);

            onProcessCompleted();
        }        
    }; 

    onProcessedImage(data, args, duration) {
        const d = args.d;
        const tx = args.tx;
        const imageBytes = args.imageBytes;
        const onProcessCompleted = args.onProcessCompleted;
        
        duration = data.analysisRoundTripMs;
        
        if(this.hasSinglePrediction) {
            data.predictions = [data];
        }

        if(data.predictions !== null && data.predictions.length > 0) {
            const predictions = data.predictions.map(p => this.getPrediction(p)).filter(p => !!p);
            const moduleReasonKeys = Object.keys(MODULES_REASON);
            const reason = moduleReasonKeys.includes(this.id) ? MODULES_REASON[this.id] : this.id;

            const isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1';
            const width = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y);
            const height = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x);

            this.postProcessingLog(d, predictions, duration);

            const eventData = {
                f: EVENT_TYPES.Trigger,
                id: d.id,
                ke: d.ke,
                details: {
                    plug: pluginConfig.plug,
                    name: d.id,
                    reason: reason,
                    matrices: predictions,
                    imgHeight: width,
                    imgWidth: height,
                    time: duration
                },
                frame: imageBytes          
            };

            tx(eventData);
        } else {
            this.postProcessingLog(d, [], duration);
        }

        onProcessCompleted();
    }

    getPrediction(prediction) {
        if(prediction === undefined) {
            return null;
        }

        const tag = this.predictionKey === null ? null : prediction[this.predictionKey];

        const confidence = prediction.confidence ?? 0;
        const y_min = prediction.y_min ?? 0;
        const x_min = prediction.x_min ?? 0;
        const y_max = prediction.y_max ?? 0;
        const x_max = prediction.x_max ?? 0;
        const width = x_max - x_min;
        const height = y_max - y_min;

        const obj = {
            x: x_min,
            y: y_min,
            width: width,
            height: height,
            tag: tag,
            confidence: confidence
        };

        this.getExtendedPrediction(obj);        

        return obj;
    };

    getExtendedPrediction(_predictionData) {
        return;
    }

    postProcessingLog(d, predictions, duration) {
        if(predictions.length > 0) {
            const messages = predictions.map(f => `${f.tag} [${(f.confidence * 100).toFixed(2)}]`);

            const message = messages.join(", ");

            this.logInfo(`${d.id} detected ${this.name}: ${message}, Response time: ${duration} ms`);

        } else {
            this.logInfo(`${d.id} did not find ${this.name} any object, Response time: ${duration} ms`);
        }
    }

    getRequestData(endpoint, additionalParameters) {
        const formData = {};

        if(this.apiKey) {
            formData[API_KEY] = this.apiKey;
        }

        if(additionalParameters !== undefined && additionalParameters !== null) {
            const keys = Object.keys(additionalParameters);

            keys.forEach(k => formData[k] = additionalParameters[k]);
        }

        const requestData = {
            url: `${this.baseUrl}${endpoint}`,
            time: true,
            formData: formData
        };

        return requestData;
    }
}

class SceneClassifierModule extends AIModule {
    constructor(moduleDetails, serverConfig) {
        super(moduleDetails, serverConfig);

        this.hasSinglePrediction = true;
        this.hasMinimumConfidence = false;
    }
};

class FaceRecognitionModule extends AIModule {
    shinobiFaces = null;
    shinobiFacePath = null;
    moduleFaces = null;
    compareInstanceId = null;

    constructor(moduleDetails, serverConfig) {
        const key = PREDICTION_KEYS.UserID;
        const processingEndpoint = FACE_RECOGNITION_ENPOINTS.Processing;

        super(moduleDetails, serverConfig, processingEndpoint, key);

        this.shinobiFaces = null;
        this.moduleFaces = null;
        this.shinobiFacePath = null;
        this.compareInstanceId = null;

        this.list();
    }

    getExtendedPrediction(predictionData) {
        const tag = predictionData.tag;

        if(tag.includes(DELIMITER)) {
            const shinobiFileParts = tag.split(DELIMITER);

            predictionData.person = shinobiFileParts[0];
        } else {
            predictionData.person = tag;
        }
    }

    postProcessingLog(d, predictions, duration) {
        const identified = predictions.filter(p => p.tag !== FACE_UNKNOWN);
        const unknownCount = predictions.length - identified.length;
        
        if(predictions.length === 0) {
            this.logInfo(`${d.id} did not find any face, Response time: ${duration} ms`);
        }

        if(unknownCount > 0) {
            this.logInfo(`${d.id} detected ${unknownCount} unknown faces, Response time: ${duration} ms`);
        }

        if(identified.length > 0) {
            const messages = identified.map(f => `${f.tag} (${f.person}) [${(f.confidence * 100).toFixed(2)}]`);
            
            const message = messages.join(", ");

            this.logInfo(`${d.id} detected faces: ${message}, Response time: ${duration} ms`);
        }
    }

    list() {
        this.logInfo("listing");

        const requestData = this.getRequestData(FACE_RECOGNITION_ENPOINTS.List);

        this.httpPost(requestData, (data, args, duration) => {
            this.moduleFaces = data.faces;
        });
    };

    register(userId) {
        this.logInfo(`Register image '${userId}'`);

        const shinobiFileParts = userId.split(DELIMITER);
        const faceName = shinobiFileParts[0];
        const imagePath = shinobiFileParts[1];
        
        const frameLocation = `${this.shinobiFacePath}${faceName}/${imagePath}`;

        const imageStream = fs.createReadStream(frameLocation);
            
        const form = {
            image: imageStream,
            userid: userId
        };
        
        const requestData = this.getRequestData(FACE_RECOGNITION_ENPOINTS.Register, form);

        this.httpPost(requestData)
    };

    unregister(userId) {
        this.logInfo(`Unregister image '${userId}'`);

        if (userId === null) {
            return;
        }
    
        const form = {
            userid: userId
        };
        
        const requestData = this.getRequestData(FACE_RECOGNITION_ENPOINTS.Unregister, form);
    
        this.httpPost(requestData)
    };

    compare(shinobiFaces, shinobiFacePath) {        
        this.shinobiFacePath = shinobiFacePath;

        if(shinobiFaces === this.shinobiFaces) {
            return;
        }

        const compareInstanceId = this.compareInstanceId || null;
        
        if (compareInstanceId !== null) {
            clearTimeout(compareInstanceId)
        }
    
        if(this.moduleFaces === null || shinobiFaces === null) {
            const that = this;
            this.compareInstanceId = setTimeout(that.compareShinobiVSServer, 5000, shinobiFaces, shinobiFacePath);
            logWarn("AI Server not ready yet, will retry in 5 seconds");
    
            return;
        }

        this.compareInstanceId = null;
    
        const shinobiFaceKeys = Object.keys(shinobiFaces);    
    
        const shinobiFiles = shinobiFaceKeys.length === 0 ? 
                                [] :
                                shinobiFaceKeys
                                    .map(faceName => {
                                        const value = shinobiFaces[faceName].map(image => this.getServerFileNameByShinobi(faceName, image));
                                
                                        return value;
                                    })
                                    .reduce((acc, item) => {
                                        const result = [...acc, ...item];
                                        
                                        return result;
                                    });
    
        const facesToRegister = shinobiFiles.filter(f => !this.moduleFaces.includes(f));
        const facesToUnregister = this.moduleFaces.filter(f => !shinobiFiles.includes(f));
    
        if(facesToRegister.length > 0) {
            this.logInfo(`Registering the following faces: ${facesToRegister}`);
            facesToRegister.forEach(f => this.register(f));
        }
    
        if(facesToUnregister.length > 0) {
            this.logInfo(`Unregister the following faces: ${facesToUnregister}`);

            facesToUnregister.forEach(f => this.unregister(f));
        }
    
        if(facesToRegister.length + facesToUnregister.length > 0) {
            this.list();
        } 
    };

    getServerFileNameByShinobi(name, image) {
        const fileName = `${name}${DELIMITER}${image}`;
    
        return fileName;
    }
}

class AIManager extends BaseClass {
    modules = null;
    serverConfig = null;
    system = null;
    fileHandlers = null;
    eventMappers = null;

    constructor() {
        super(`AIManager`);

        this.fileHandlers = {};
        this.modules = [];
        this.system = null;
        
        const webProtocol = PROTOCOLS[pluginConfig.api.isSSL];
    
        const baseUrl = `${webProtocol}://${pluginConfig.api.host}:${pluginConfig.api.port}/v1`;
        const apiKey = pluginConfig.api.apiKey;

        this.serverConfig = {
            baseUrl: baseUrl,
            apiKey: apiKey
        }

        this.eventMappers = {};

        this.loadModules();

        this.attachToShinobi();
    }

    attachToShinobi() {
        const basePlugins = [];
        
        const isWorker = workerData && workerData.ok === true;
        const pluginBasePath = isWorker ? "pluginWorkerBase.js" : "pluginBase.js";

        for (let index = 0; index < MAX_ATTEMPTS_LOAD_BASE; index++) {
            try {
                const s = require(`../${pluginBasePath}`)(__dirname, pluginConfig);

                basePlugins.push(s);

                break;
            } catch (error) {
                this.logError(`Failed to load  ${pluginBasePath}, Error: ${error}`);
            }            
        }

        if(basePlugins.length > 0) {
            this.system = basePlugins[0];

            if(!isWorker) {
                const { haltMessage, checkStartTime, setStartTime } = require('../pluginCheck.js');
            
                if(!checkStartTime()) {
                    console.log(haltMessage, new Date());

                    this.system.disconnectWebSocket();
                }
            
                setStartTime();
            }

            this.eventMappers[EVENT_TYPES.RecompileFaceDescriptors] = (d, cn, tx) => this.onRecompileFaceDescriptors(d, cn, tx);

            this.system.detectObject = (frameBuffer, d, tx, frameLocation, callback) =>  this.onDetectObject(frameBuffer, d, tx, frameLocation, callback);
            this.system.onPluginEvent.push((d, cn, tx) => this.handlePluginEvent(d, cn, tx));
        }         
    }

    loadModules() {
        const ts = new Date().getTime();
        const endpoint = `${AI_SERVER_MANAGEMENT_ENDPOINTS.ListStatus}?random=${ts}`;

        this.getDetailsFromAIServer(endpoint, (err, _res, body) => {
            const data = JSON.parse(body);

            const success = data.success;

            if(success) {
                this.logInfo(`Loaded modules, Data: ${JSON.stringify(data.statuses)}`);

                data.statuses.forEach(m => this.loadModule(m));
            } else {
                this.logError(`Failed to load modules, Error: ${data.error}`);                
            }
        });
    }

    loadModule(moduleDetails) {
        const endpoint = `${AI_SERVER_MANAGEMENT_ENDPOINTS.Settings}/${moduleDetails.moduleId}`;

        this.getDetailsFromAIServer(endpoint, (err, _res, body) => {
            const data = JSON.parse(body);

            const success = data.success;

            if(success) {
                this.logInfo(`Loaded module '${moduleDetails.moduleId}', Data: ${JSON.stringify(data.settings)}`);

                this.loadAIModule(moduleDetails, data.settings);
            } else {
                this.logError(`Failed to load module '${moduleDetails.moduleId}', Error: ${data.error}`);  
            }
        });
    }

    getDetailsFromAIServer(endpoint, callback) {
        const url = `${this.serverConfig.baseUrl}${endpoint}`;

        request.get(url, callback);
    }

    loadAIModule(moduleDetails, moduleSettings) {
        const wasActivated = moduleSettings.activate;

        const moduleId = moduleDetails.moduleId;

        const moduleInitializersKeys = Object.keys(MODULE_INITIALIZERS);
        
        const moduleInitializer = moduleInitializersKeys.includes(moduleId) ? 
            MODULE_INITIALIZERS[moduleId] : 
            DEFAULT_MODULE_INITIALIZER;

        const moduleInstance = moduleInitializer(moduleDetails, this.serverConfig);

        moduleInstance.isActive = moduleInstance.isActive && wasActivated;

        this.modules.push(moduleInstance); 
    }

    onFrameProcessed(d, framePath) {
        this.fileHandlers[framePath] -= 1;

        if(this.fileHandlers[framePath] === 0) {
            if(fs.existsSync(framePath)) {
                fs.unlink(framePath, err => {
                    if(err) {
                        this.logError(`Failed to delete '${framePath}' for ${d.id}, Error: ${err}`);
                    } 
                });
            }
        }
    }

    onFrameReady(d, tx, frameBuffer, framePath, err) {
        if(err) {
            return this.system.systemLog(err);
        }
    
        try {
            if (this.modules !== null) {
                const activeModules = this.modules.filter(m => m.isActive);

                this.fileHandlers[framePath] = activeModules.length;

                activeModules.forEach(m => m.process(d, tx, frameBuffer, framePath, () => this.onFrameProcessed(d, framePath)));
            }

        } catch(ex) {
            this.logError(`Failed to handle frame, Error: ${ex}`);
        }
    }

    onDetectObject(frameBuffer, d, tx, frameLocation, callback) {
        const dirCreationOptions = {
            recursive: true
        };
    
        d.dir = `${this.system.dir.streams}${d.ke}/${d.id}/`;
        
        const framePath = `${d.dir}${this.system.gid(5)}.jpg`;
    
        if(!fs.existsSync(d.dir)) {
            fs.mkdirSync(d.dir, dirCreationOptions);
        }

        fs.writeFile(framePath, frameBuffer, (err) => this.onFrameReady(d, tx, frameBuffer, framePath, err));

        callback();
    }

    onRecompileFaceDescriptors(d, _cn, _tx) {
        this.logInfo("Handinling recompile face descriptors event");

        const relevantModules = this.modules
                                    .filter(m => m.id === "FaceProcessing");

        const faceRecognitionModule = relevantModules.length === 0 ? null : relevantModules[0];

        if(faceRecognitionModule !== null) {
            faceRecognitionModule.compare(d.faces, d.path);
        }
    };

    handlePluginEvent(d, cn, tx) {       
        const eventMapperKeys = Object.keys(this.eventMappers);

        if(eventMapperKeys.includes(d.f)) {
            const eventMapper = this.eventMappers[d.f];

            eventMapper(d, cn, tx);
        }
    }
}

const manager = new AIManager();