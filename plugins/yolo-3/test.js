// Base Init >>
var fs = require('fs').promises;
const fetch = require('node-fetch');
// Base Init />>
var yolo = require('node-yolo-shinobi');//this is @vapi/node-yolo@1.2.4 without the console output for detection speed
var detector = new yolo(__dirname + "/models", "cfg/coco.data", "cfg/yolov3.cfg", "yolov3.weights");

async function runDetection(buffer){
    var timeStart = new Date()
    const tempName = `${new Date().getTime()}.jpg`
    const fullPath = `${process.cwd()}/${tempName}`
    try{
        await fs.writeFile(fullPath,buffer)
    }catch(error){
        console.error(`await fs.writeFile`,error);
    }
    try{
        const detections = await detector.detect(fullPath)
        matrices = []
        detections.forEach(function(v){
            matrices.push({
              x:v.box.x,
              y:v.box.y,
              width:v.box.w,
              height:v.box.h,
              tag:v.className,
              confidence:v.probability,
            })
        })
        await fs.rm(fullPath)
        return matrices
    }catch(error){
        console.error(`await detector.detect`,error);
    }
    return []
}

const testImageUrl = `https://cdn.shinobi.video/images/test/car.jpg`
const testImageUrl2 = `https://cdn.shinobi.video/images/test/bear.jpg`
const testImageUrl3 = `https://cdn.shinobi.video/images/test/people.jpg`
const runTest = async (imageUrl) => {
    console.log(`Loading ${imageUrl}`)
    const response = await fetch(imageUrl);
    const frameBuffer = await response.buffer();
    console.log(`Detecting upon ${imageUrl}`)
    const results = await runDetection(frameBuffer)
    if(results[0]){
        var mats = []
        console.log('Detected Objects!')
        results.forEach(function(matrix){
            console.log(matrix)
        })
    }else{
        console.log('No Matrices...')
    }
    console.log(`Done ${imageUrl}`)
}
const allTests = async () => {
    await runTest(testImageUrl)
    await runTest(testImageUrl2)
    await runTest(testImageUrl3)
}
allTests()
