if [ -x "$(command -v apt)" ]; then
    apt install build-essential -y
    apt install gcc-7 g++-7 -y
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 50
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 50
else
    echo 'This script will not operate on this Operating System.'
    echo 'This OS does not use "apt".'
fi
echo "Done!"
gcc --version
g++ --version
