//
// Shinobi - Yolo V5 Plugin
// Copyright (C) 2023 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
var fs = require('fs').promises;
var config = require('./conf.json')
var s
const {
  workerData
} = require('worker_threads');
if(workerData && workerData.ok === true){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
// Base Init />>
const {
    drawDetections,
    loadClassNames,
    loadNet,
    detect
} = require('./libs/addon.js');
// var yolo = require('@vapi/node-yolo');
async function main(){
    const isCuda = config.gpu || false
    const pathToOnnxModel = config.pathToOnnxModel || `${__dirname}/config_files/yolov5s.onnx`
    const pathToClassesFile = config.pathToClassesFile || `${__dirname}/config_files/classes.txt`
    const classNames = await loadClassNames(pathToClassesFile)
    await loadNet(isCuda,pathToOnnxModel)
    s.detectObject = async function(buffer,d,tx,frameLocation,callback){
        const timeStart = new Date()
        try{
            const detections = await detect(buffer,{
                inputWidth: 640.0,
                inputHeight: 640.0,
                scoreThreshold: 0.2,
                nmsThreshold: 0.4,
                confidenceThreshold: 0.4,
            });
            var matrices = [];
            detections.forEach(function(v){
                matrices.push({
                  x: v.box.x,
                  y: v.box.y,
                  width: v.box.width,
                  height: v.box.height,
                  tag: classNames[v.class_id],
                  confidence: v.confidence,
                })
            })
            if(matrices.length > 0){
                var isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1'
                var width = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
                var height = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)
                tx({
                    f: 'trigger',
                    id: d.id,
                    ke: d.ke,
                    details: {
                        plug: config.plug,
                        name: 'yolov5',
                        reason: 'object',
                        matrices: matrices,
                        imgHeight:width,
                        imgWidth:height,
                        time: (new Date()) - timeStart
                    },
                    frame: buffer
                })
            }
        }catch(error){
            console.error(`await detect`,error);
        }
        callback()
    }
}
main()
