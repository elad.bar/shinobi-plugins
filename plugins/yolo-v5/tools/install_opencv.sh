#!/bin/bash

if [ -d "/usr/local/include/opencv4" ] || [ -d "/usr/include/opencv4" ]; then
    echo "OpenCV Build libraries found!"
    exit 0
fi

# Detect package manager
if command -v dnf > /dev/null 2>&1; then
    PKG_MANAGER="dnf"
elif command -v yum > /dev/null 2>&1; then
    PKG_MANAGER="yum"
elif command -v apt > /dev/null 2>&1; then
    PKG_MANAGER="apt"
else
    echo "Could not find a supported package manager (dnf, yum, or apt). Exiting."
    exit 1
fi

check_opencv_in_package_manager() {
    case $PKG_MANAGER in
    "dnf" | "yum")
        if $PKG_MANAGER list opencv* &>/dev/null; then
            return 0
        else
            return 1
        fi
        ;;
    "apt")
        if apt-cache search libopencv-core-dev | grep 'libopencv'; then
            return 0
        else
            return 1
        fi
        ;;
    esac
    return 1
}

if check_opencv_in_package_manager; then
    echo "OpenCV is available in the package manager. Do you want to install it from there? (y/N). "
    echo "If you want to install with NVIDIA GPU Support say (N)o."
    echo "(y)es or (N)o"
    usePackageManager=""
    read usePackageManager
    if [ "$usePackageManager" = "y" ] || [ "$usePackageManager" = "Y" ]; then
        echo "OpenCV installed via package manager."
        if [ "$PKG_MANAGER" = "apt" ]; then
            sudo apt update -y
            sudo apt install libopencv-dev libopencv-core-dev libopencv-imgproc-dev libopencv-imgcodecs-dev libopencv-highgui-dev libopencv-dnn-dev -y
        else
            sudo $PKG_MANAGER groupinstall "Development Tools" -y
            sudo $PKG_MANAGER install opencv opencv-devel opencv-core opencv-imgproc opencv-highgui opencv-dnn -y
        fi
        if [ -f /usr/local/include/opencv4 ]; then
            echo "Looks like OpenCV may have been installed from source."
            echo "NOT linking '/usr/include/opencv4/' to '/usr/local/include/opencv4'"
        else
            ln -s /usr/include/opencv4/ /usr/local/include/opencv4
        fi
        exit 0
    fi
fi

# Update package lists
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt update && sudo apt upgrade -y
else
    sudo $PKG_MANAGER update -y
fi

# Install development tools and cmake
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install build-essential cmake wget unzip pkg-config gcc-10 g++-10 libgcc-10-dev -y
else
    sudo $PKG_MANAGER groupinstall "Development Tools" -y
    sudo $PKG_MANAGER install cmake wget unzip pkg-config -y
fi

# Install image I/O packages
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install libpng-dev libopenexr-dev libwebp-dev libjpeg-dev -y
else
    sudo $PKG_MANAGER install libpng-devel jasper-devel openexr-devel libwebp-devel libjpeg-turbo-devel -y
fi

# Install video I/O packages
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install libv4l-dev libgstreamer1.0-dev -y
else
    sudo $PKG_MANAGER install libdc1394-devel libv4l-devel gstreamer1-devel -y
fi

# Install additional video and GUI packages (issue 2, 4, 5, 6)
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install libgtkglext1-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev gstreamer1.0-plugins-* libdc1394-22-dev -y
else
    sudo $PKG_MANAGER install gtkglext-devel ffmpeg-devel gstreamer-plugins-* libdc1394-devel -y
fi

# Install optimization packages
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install libtbb-dev libeigen3-dev -y
else
    sudo $PKG_MANAGER install tbb-devel eigen3-devel -y
fi

# Install protobuf
if [ "$PKG_MANAGER" = "apt" ]; then
    sudo apt install libprotobuf-dev protobuf-compiler -y
else
    sudo $PKG_MANAGER install protobuf-devel protobuf-compiler -y
fi
# Navigate to home directory and download OpenCV and OpenCV contrib
wget -O opencv.zip https://github.com/opencv/opencv/archive/refs/tags/4.8.0.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/refs/tags/4.8.0.zip

# Unzip downloaded files
unzip opencv.zip
unzip opencv_contrib.zip

# Build and install OpenCV
cd opencv-4.8.0
mkdir build && cd build

echo "Install GPU Support for NVIDIA GPUs?"
echo "NVIDIA Drivers, CUDA Toolkit and CUDNN need to be installed already."
echo "(y)es or (N)o"
read doGpuInstalled
if [ "$doGpuInstalled" = "y" ] || [ "$doGpuInstalled" = "Y" ]; then
    cmake -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_NVCUVID=ON -D FORCE_VTK=ON -D WITH_XINE=ON -D WITH_CUDA=ON -D WITH_OPENGL=ON -D WITH_TBB=ON -D WITH_OPENCL=ON -D CMAKE_BUILD_TYPE=RELEASE -D CUDA_NVCC_FLAGS="-D_FORCE_INLINES --expt-relaxed-constexpr" -D WITH_GDAL=ON -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.8.0/modules/ -D ENABLE_FAST_MATH=1 -D CUDA_FAST_MATH=1 -D WITH_CUBLAS=1 -D CXXFLAGS="-std=c++11" -DCMAKE_CXX_COMPILER=g++-10 -DCMAKE_C_COMPILER=gcc-10 ..
else
    cmake -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D FORCE_VTK=ON \
    -D WITH_XINE=ON \
    -D WITH_OPENGL=ON \
    -D WITH_TBB=ON \
    -D WITH_OPENCL=ON \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D WITH_GDAL=ON \
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.8.0/modules/ \
    -D CXXFLAGS="-std=c++11" \
    -DCMAKE_CXX_COMPILER=g++-10 \
    -DCMAKE_C_COMPILER=gcc-10 ..
fi

make -j$(nproc)
sudo make install

# Add OpenCV to environment variables
if [ -f ~/.bashrc ]; then
    echo "export PKG_CONFIG_PATH=\$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig" >> ~/.bashrc
    echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:/usr/local/lib" >> ~/.bashrc
    source ~/.bashrc
else
    echo "~/.bashrc does not exist. Creating one and setting environment variables."
    touch ~/.bashrc
    echo "export PKG_CONFIG_PATH=\$PKG_CONFIG_PATH:/usr/local/lib/pkg
