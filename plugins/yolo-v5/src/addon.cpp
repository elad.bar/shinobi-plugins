// Copyright (C) 2023 Moe Alam, Shinobi Systems

#include <fstream>
#include <napi.h>
#include <opencv2/opencv.hpp>

using namespace cv;

// Function Definitions

std::vector<std::string> load_class_list(const std::string& path = "config_files/classes.txt") {
    std::vector<std::string> class_list;
    std::ifstream ifs(path);
    std::string line;
    while (getline(ifs, line)) {
        class_list.push_back(line);
    }
    return class_list;
}

void load_net(cv::dnn::Net &net, bool is_cuda, const std::string& model_path = "config_files/yolov5s.onnx") {
    auto result = cv::dnn::readNet(model_path);
    if (is_cuda) {
        std::cout << "Attempting to use CUDA\n";
        result.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
        result.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA_FP16);
    } else {
        std::cout << "Running on CPU\n";
        result.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
        result.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
    }
    net = result;
}

void releaseNet(cv::dnn::Net &net) {
    net.~Net();
}

const std::vector<cv::Scalar> colors = {cv::Scalar(255, 255, 0), cv::Scalar(0, 255, 0), cv::Scalar(0, 255, 255), cv::Scalar(255, 0, 0)};

cv::Mat format_yolov5(const cv::Mat &source) {
    int col = source.cols;
    int row = source.rows;
    int _max = std::max(col, row);
    cv::Mat result = cv::Mat::zeros(_max, _max, CV_8UC3);
    source.copyTo(result(cv::Rect(0, 0, col, row)));
    return result;
}

struct Detection {
    int class_id;
    float confidence;
    cv::Rect box;
};

// Utility function: Convert std::vector<Detection> to Napi::Array of Napi::Objects
Napi::Array detections_to_napi_array(const Napi::Env& env, const std::vector<Detection>& detections) {
    Napi::Array napiDetections = Napi::Array::New(env, detections.size());
    for (size_t i = 0; i < detections.size(); i++) {
        Napi::Object obj = Napi::Object::New(env);
        obj.Set("class_id", Napi::Number::New(env, detections[i].class_id));
        obj.Set("confidence", Napi::Number::New(env, detections[i].confidence));

        Napi::Object rect = Napi::Object::New(env);
        rect.Set("x", Napi::Number::New(env, detections[i].box.x));
        rect.Set("y", Napi::Number::New(env, detections[i].box.y));
        rect.Set("width", Napi::Number::New(env, detections[i].box.width));
        rect.Set("height", Napi::Number::New(env, detections[i].box.height));
        obj.Set("box", rect);

        napiDetections[i] = obj;
    }
    return napiDetections;
}

// Global network instance
cv::dnn::Net globalNet;
std::vector<std::string> classNames;

void LoadNetWrapped(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    if(info.Length() < 2 || !info[0].IsBoolean() || !info[1].IsString()) {
        Napi::TypeError::New(env, "Expected arguments: (boolean, string)").ThrowAsJavaScriptException();
        return;
    }

    bool is_cuda = info[0].As<Napi::Boolean>();
    std::string model_path = info[1].As<Napi::String>();

    load_net(globalNet, is_cuda, model_path);
}

Napi::Value LoadClassNamesWrapped(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    if(info.Length() < 1 || !info[0].IsString()) {
        Napi::TypeError::New(env, "Path to classes file expected").ThrowAsJavaScriptException();
        return env.Null();
    }

    std::string path = info[0].As<Napi::String>();
    classNames = load_class_list(path);
    Napi::Array napiClassNames = Napi::Array::New(env, classNames.size());
    for (size_t i = 0; i < classNames.size(); i++) {
        napiClassNames.Set(i, Napi::String::New(env, classNames[i]));
    }

    return napiClassNames;
}


Napi::Array DetectWrapped(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    // Decode the image
    Napi::Buffer<uchar> buffer = info[0].As<Napi::Buffer<uchar>>();
    cv::Mat image = cv::imdecode(cv::Mat(1, buffer.Length(), CV_8UC1, buffer.Data()), IMREAD_COLOR);

    // Extract options
    Napi::Object options = info[1].As<Napi::Object>();
    float inputWidth = options.Get("inputWidth").As<Napi::Number>().FloatValue();
    float inputHeight = options.Get("inputHeight").As<Napi::Number>().FloatValue();
    float scoreThreshold = options.Get("scoreThreshold").As<Napi::Number>().FloatValue();
    float nmsThreshold = options.Get("nmsThreshold").As<Napi::Number>().FloatValue();
    float confidenceThreshold = options.Get("confidenceThreshold").As<Napi::Number>().FloatValue();

    // Previous detect function contents
    cv::Mat blob;
    auto input_image = format_yolov5(image);

    cv::dnn::blobFromImage(input_image, blob, 1./255., cv::Size(inputWidth, inputHeight), cv::Scalar(), true, false);
    globalNet.setInput(blob);
    std::vector<cv::Mat> outputs;
    globalNet.forward(outputs, globalNet.getUnconnectedOutLayersNames());

    float x_factor = input_image.cols / inputWidth;
    float y_factor = input_image.rows / inputHeight;

    float *data = (float *)outputs[0].data;
    const int dimensions = 85;
    const int rows = 25200;

    std::vector<int> class_ids;
    std::vector<float> confidences;
    std::vector<cv::Rect> boxes;

    for (int i = 0; i < rows; ++i) {
        float confidence = data[4];
        if (confidence >= confidenceThreshold) {
            float * classes_scores = data + 5;
            cv::Mat scores(1, classNames.size(), CV_32FC1, classes_scores);
            cv::Point class_id;
            double max_class_score;
            // std::cout << "Scores: " << scores << std::endl;

            minMaxLoc(scores, 0, &max_class_score, 0, &class_id);
            if (max_class_score > scoreThreshold) {
                confidences.push_back(confidence);
                class_ids.push_back(class_id.x);
                // std::cout << "Max Class Score: " << max_class_score << ", Class ID: " << class_id.x << std::endl;
                float x = data[0];
                float y = data[1];
                float w = data[2];
                float h = data[3];
                int left = int((x - 0.5 * w) * x_factor);
                int top = int((y - 0.5 * h) * y_factor);
                int width = int(w * x_factor);
                int height = int(h * y_factor);
                boxes.push_back(cv::Rect(left, top, width, height));
            }
        }
        data += dimensions;
    }

    std::vector<int> nms_result;
    cv::dnn::NMSBoxes(boxes, confidences, scoreThreshold, nmsThreshold, nms_result);

    std::vector<Detection> detections;
    for (int i = 0; i < nms_result.size(); i++) {
        int idx = nms_result[i];
        Detection result;
        result.class_id = class_ids[idx];
        result.confidence = confidences[idx];
        result.box = boxes[idx];
        detections.push_back(result);
    }

    // Convert detections to Napi::Array
    Napi::Array napiDetections = Napi::Array::New(env, detections.size());
    for (size_t i = 0; i < detections.size(); i++) {
        Napi::Object obj = Napi::Object::New(env);
        obj.Set("class_id", Napi::Number::New(env, detections[i].class_id));
        obj.Set("confidence", Napi::Number::New(env, detections[i].confidence));

        Napi::Object rect = Napi::Object::New(env);
        rect.Set("x", Napi::Number::New(env, detections[i].box.x));
        rect.Set("y", Napi::Number::New(env, detections[i].box.y));
        rect.Set("width", Napi::Number::New(env, detections[i].box.width));
        rect.Set("height", Napi::Number::New(env, detections[i].box.height));
        obj.Set("box", rect);

        napiDetections[i] = obj;
    }

    return napiDetections;
}

Napi::Value DrawDetectionsWrapped(const Napi::CallbackInfo &info) {
    Napi::Env env = info.Env();

    // Decode the image
    Napi::Buffer<uchar> buffer = info[0].As<Napi::Buffer<uchar>>();
    cv::Mat image = cv::imdecode(cv::Mat(1, buffer.Length(), CV_8UC1, buffer.Data()), cv::IMREAD_COLOR);

    // Get detections array
    Napi::Array napiDetections = info[1].As<Napi::Array>();

    for (size_t i = 0; i < napiDetections.Length(); i++) {
        Napi::Object detectionObj = napiDetections.Get(i).As<Napi::Object>();

        int class_id = detectionObj.Get("class_id").As<Napi::Number>().Int32Value();
        Napi::Object rectObj = detectionObj.Get("box").As<Napi::Object>();
        cv::Rect box(rectObj.Get("x").As<Napi::Number>().Int32Value(),
                     rectObj.Get("y").As<Napi::Number>().Int32Value(),
                     rectObj.Get("width").As<Napi::Number>().Int32Value(),
                     rectObj.Get("height").As<Napi::Number>().Int32Value());

        // Draw bounding box and class label on the image
        cv::rectangle(image, box, colors[class_id % colors.size()], 2);
        cv::putText(image, classNames[class_id], cv::Point(box.x, box.y - 10),
                    cv::FONT_HERSHEY_SIMPLEX, 0.5, colors[class_id % colors.size()], 2);
    }

    // Encode the modified image into a buffer
    std::vector<uchar> encodedBuf;
    cv::imencode(".jpg", image, encodedBuf);
    Napi::Buffer<uchar> napiBuffer = Napi::Buffer<uchar>::Copy(env, encodedBuf.data(), encodedBuf.size());

    return napiBuffer;
}

Napi::Value ReleaseNetWrapped(const Napi::CallbackInfo& info) {
    releaseNet(globalNet);
    return Napi::Value();
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(Napi::String::New(env, "loadNet"), Napi::Function::New(env, LoadNetWrapped));
    exports.Set(Napi::String::New(env, "loadClassNames"), Napi::Function::New(env, LoadClassNamesWrapped));
    exports.Set(Napi::String::New(env, "detect"), Napi::Function::New(env, DetectWrapped));
    exports.Set(Napi::String::New(env, "drawDetections"), Napi::Function::New(env, DrawDetectionsWrapped));
    exports.Set(Napi::String::New(env, "releaseNet"), Napi::Function::New(env, ReleaseNetWrapped));
    return exports;
}

NODE_API_MODULE(addon, Init)
