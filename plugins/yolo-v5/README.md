# Yolo V5 N-API Module

This is a C++ based module for YoloV5 Object Detection and a plugin for Shinobi (https://shinobi.video).

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **Plugin Manager** tab.
3. In the listing select the latest **"Yolo"** plugin to Download.
4. Hit `Run Installer` for the newly downloaded plugin.
    - This plugin's installer requires a system reboot if you choose to install Coral TPU Drivers.
5. Once Installed click "Enable" and restart Shinobi.
6. Reopen the Plugin Manager and run the `Test Object Detector` command.
7. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)

Your test should look similar to this.

```
Frame 176 detected in 0.278s
[
  {
    class_id: 0,
    confidence: 0.6456838250160217,
    box: { x: 254, y: 432, width: 58, height: 48 },
    tag: 'person'
  }
]
Frame 177 detected in 0.276s
[
  {
    class_id: 0,
    confidence: 0.6623225212097168,
    box: { x: 256, y: 433, width: 54, height: 52 },
    tag: 'person'
  }
]
Test done in 47.241s!
#END_PROCESS
```

## Using GPU (NVIDIA Only)

in your plugin's conf.json add `"gpu": true,`

[Learn more about Downloading and Installing Plugins here](https://docs.shinobi.video/plugin/install)
