$(document).ready(function(){
    var faceManagerModal = $('#faceManager')
    var faceManagerImageContainer = $('#faceManagerImageContainer')
    var faceManagerImages = $('#faceManagerImages')
    var faceManagerUnknownFaces = $('#faceManagerUnknownFaces')
    var faceManagerForm = $('#faceManagerUploadForm')
    var faceNameField = $('#faceNameField')
    var getFaceImages = function(callback){

        $.get(getApiPrefix('faceManager') + '/images',function(response){
            callback(response || [])
        })
    }
    var deleteFaceImage = function(name,image,callback){
        $.get(getApiPrefix('faceManager') + '/image/' + name + '/' + image + '/delete',function(response){
            callback(response)
        })
    }
    var deleteFaceFolder = function(name,callback){
        $.get(getApiPrefix('faceManager') + '/delete/' + name,function(response){
            callback(response)
        })
    }
    var createFaceFolder = function(name,callback){
        $.get(getApiPrefix('faceManager') + '/create/' + name,function(response){
            callback(response)
        })
    }
    var moveFaceImage = function(name,image,newFaceName,callback){
        $.get(getApiPrefix('faceManager') + '/image/' + name + '/' + image + '/move/' + newFaceName + '/' + image + '?websocketResponse=1' ,function(response){
            if(callback)callback(response)
        })
    }
    var getFaceImageHtml = function(name,image){
        console.log(name,image)
        return `<div class="col-${name.indexOf('UNKN_') > -1 ? '12' : '3'} p-2 face-image" face="${name}" image="${image}">
           <div class="face-image-bg" style="background-image:url('${getApiPrefix('faceManager')}/image/${name}/${image}')">
               <div class="controls row m-0">
                   <div class="col p-0">
                        <a href="#" class="btn btn-sm btn-danger m-0 delete"><i class="fa fa-trash-o"></i></a>
                   </div>
                   <div class="col p-0 text-right">
                        <span class="badge badge-sm bg-dark pull-right">${name}</span>
                   </div>
               </div>
               <div class="controls-bottom">
                   <span class="badge badge-sm bg-dark">${image}</span>
               </div>
           </div>
       </div>`
    }
    var createFaceHeader = function(name){
        return `<div face="${name}" class="mt-4 mb-2 face-title"><a class="load-name">${name}</a> <a class="btn btn-danger btn-sm deleteFolder" face="${name}"><i class="fa fa-trash-o"></i></a></div>`
    }
    var drawFaceFolder = function(name){
        if(faceManagerImages.find(`[face="${name}"]`).length === 0){
            faceManagerImages.append(`${createFaceHeader(name)}<div class="row face-container" face="${name}"></div>`)
            activateDroppableContainer(name)
        }
    }
    var drawFaceImages = function(){
        getFaceImages(function(response){
            var faces = response.faces
            var unknownFaces = response.unknownFaces
            var html = ''
            $.each(faces,function(name,images){
                // if(images.length === 0)return
                html += `${createFaceHeader(name)}<div class="row face-container" face="${name}">`
                $.each(images,function(n,image){
                    if(image.indexOf('.faceDescriptor') > -1)return;
                    html += getFaceImageHtml(name,image)
                })
                html += `</div>`
            })
            faceManagerImages.html(html)
            $.each(faces,function(name,images){
                activateDroppableContainer(name)
            })
            var html = ''
            $.each(unknownFaces,function(name,images){
                // if(images.length === 0)return
                html += `${createFaceHeader(name)}<div class="col-md-12 face-container" face="${name}">`
                $.each(images,function(n,image){
                    if(image.indexOf('.faceDescriptor') > -1)return;
                    html += getFaceImageHtml(name,image)
                })
                html += `</div>`
            })
            faceManagerUnknownFaces.html(html)
            $.each(unknownFaces,function(name,images){
                activateDroppableContainer(name)
            })
            activateDraggableImages()
            prettySizeFaceImages()
        })
    }
    var prettySizeFaceImages = function(){
        var faceImagesRendered = faceManagerImageContainer.find('.face-image')
        var faceHeight = faceImagesRendered.first().width()
        faceImagesRendered.css('height',faceHeight)
    }
    var activateDroppableContainer = function(name){
        var row = faceManagerImageContainer.find(`.face-container[face="${name}"]`)
        try{
            row.droppable("destroy")
        }catch(err){

        }
        row.droppable({
            tolerance: "intersect",
            accept: ".face-image",
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            drop: function(event, ui) {
                var el = $(this)
                var newFace = el.attr('face')
                var faceImageElement = $(ui.draggable)
                var oldFace = faceImageElement.attr('face')
                var fileName = faceImageElement.attr('image')
                if(oldFace !== newFace){
                    moveFaceImage(oldFace,fileName,newFace)
                }else{
                    faceImageElement.css({
                        top: 0,
                        left: 0,
                    })
                }
            }
        })
    }
    var activateDraggableImages = function(name){
        var imageEls = faceManagerImageContainer.find(`.face-image`)
        try{
            imageEls.draggable("destroy")
        }catch(err){

        }
        imageEls.draggable({
            appendTo: "body",
            cursor: "move",
            // helper: 'clone',
            revert: "invalid"
        });
    }
    var createFaceImageBlock = function(row,faceName,fileName){
        var existingBlock = row.find(`[face="${faceName}"][image="${fileName}"]`)
        if(existingBlock.length > 0){
            existingBlock.draggable('destroy')
            existingBlock.remove()
        }
        row.prepend(getFaceImageHtml(faceName,fileName))
    }
    $(window).resize(function(){
        prettySizeFaceImages()
    })
    faceManagerImageContainer.on('click','.delete',function(e){
        e.preventDefault()
        var el = $(this).parents('.face-image')
        var faceName = el.attr('face')
        var faceImage = el.attr('image')
        $.confirm.create({
            title: lang.deleteImage,
            body: lang.deleteImageText + `<div class="mt-3"><img style="width:100%;border-radius:5px;" src="${getApiPrefix('faceManager')}/image/${faceName}/${faceImage}"></div>`,
            clickOptions: {
                class: 'btn-danger',
                title: lang.Delete,
            },
            clickCallback: function(){
                deleteFaceImage(faceName,faceImage,function(response){
                    console.log(response)
                })
            }
        })
        return false;
    })
    faceManagerImageContainer.on('click','.load-name',function(e){
        e.preventDefault()
        var el = $(this).parents('[face]')
        var faceName = el.attr('face')
        $('#faceNameField').val(faceName)
        return false;
    })
    faceManagerImageContainer.on('click','.deleteFolder',function(e){
        e.preventDefault()
        var faceName = $(this).attr('face')
        $.confirm.create({
            title: lang.deleteFace,
            body: lang.deleteFaceText,
            clickOptions: {
                class: 'btn-danger',
                title: lang.Delete,
            },
            clickCallback: function(){
                deleteFaceFolder(faceName,function(response){
                    console.log(response)
                })
            }
        })
        return false;
    })
    faceManagerForm.on('click','.createFolder',function(e){
        e.preventDefault()
        var faceName = $('#faceNameField').val()
        createFaceFolder(faceName,function(response){
            console.log(response)
            if(response.ok === true)drawFaceFolder(faceName)
        })
        return false;
    })
    $('#fileManager-fileinput').change(function(){
        var name = faceNameField.val()
        $.ajax({
          url: getApiPrefix('faceManager') + '/image/' + name,
          type: 'POST',
          data: new FormData(faceManagerForm[0]),
          cache: false,
          contentType: false,
          processData: false,
        },function(data){
            console.log(data)
        })
    })
    onWebSocketEvent((d) => {
        switch(d.f){
            case'faceManagerImageUploaded':
                var row = faceManagerImageContainer.find(`.face-container[face="${d.faceName}"]`)
                if(row.length === 0){
                    var holder = d.faceName.indexOf('UNKN_') > -1 ? faceManagerUnknownFaces : faceManagerImages
                    holder.append(`${createFaceHeader(d.faceName)}<div class="row face-container" face="${d.faceName}"></div>`)
                    row = faceManagerImageContainer.find(`.face-container[face="${d.faceName}"]`)
                    activateDroppableContainer(d.faceName)
                }
                createFaceImageBlock(row,d.faceName,d.fileName)
                activateDraggableImages()
                prettySizeFaceImages()
            break;
            case'faceManagerImageDeleted':
                $(`.face-image[face="${d.faceName}"][image="${d.fileName}"]`).remove()
            break;
            case'faceManagerFolderDeleted':
                $(`[face="${d.faceName}"]`).remove()
            break;
        }
    })
    addOnTabOpen('faceManager', function () {
        drawFaceImages()
    })
    addOnTabReopen('faceManager', function () {
        drawFaceImages()
    })
})
