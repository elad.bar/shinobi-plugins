var fs = require('fs').promises
const {
    createReadStream
} = require('fs')
var fileUpload = require('express-fileupload')
module.exports = async function(s,config,lang,app,io){
    if(!config.facesFolder || !config.unknownFacesFolder){
        return console.error('Must define `facesFolder` and `unknownFacesFolder` in conf.json!')
    }
    const facesFolder = s.checkCorrectPathEnding(config.facesFolder)
    const unknownFacesFolder = s.checkCorrectPathEnding(config.unknownFacesFolder)
    try{
        await fs.mkdir(facesFolder)
    }catch(err){}
    try{
        await fs.mkdir(unknownFacesFolder)
    }catch(err){}

    const sendDataToConnectedUsers = (data, groupKey) => {
        return s.tx(data,`GRP_${groupKey}`)
    }
    const getFaceFolderNames = async (groupKey) => {
        const folderPath = facesFolder + '/' + groupKey + '/'
        const folders = await fs.readdir(folderPath)
        var faces = []
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i]
            var stats = await fs.stat(folderPath + folder)
            if(stats.isDirectory()){
                faces.push(folder)
            }
        }
        return faces
    }
    const getUnknownFaceFolderNames = async (groupKey) => {
        const folderPath = unknownFacesFolder + '/' + groupKey + '/'
        const folders = await fs.readdir(folderPath)
        var faces = []
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i]
            var stats = await fs.stat(folderPath + folder)
            if(stats.isDirectory()){
                faces.push(folder)
            }
        }
        return faces
    }
    const getFaceImages = async (groupKey) => {
        try{
            const folderPath = facesFolder + '/' + groupKey + '/'
            const folders = await fs.readdir(folderPath)
            var faces = {}
            for (let i = 0; i < folders.length; i++) {
                let name = folders[i]
                var stats = await fs.stat(folderPath + name)
                if(stats.isDirectory()){
                    faces[name] = []
                    var images
                    try{
                        images = await fs.readdir(folderPath + name)
                    }catch(err){
                        images = []
                    }
                    for (let i = 0; i < images.length; i++) {
                        let image = images[i]
                        faces[name].push(image)
                    }
                }
            }
            return faces
        }catch(err){
            return []
        }
    }
    const getUnknownFaceImages = async (groupKey) => {
        try{
            const folderPath = unknownFacesFolder + '/' + groupKey + '/'
            const folders = await fs.readdir(folderPath)
            var faces = {}
            for (let i = 0; i < folders.length; i++) {
                let name = folders[i]
                var stats = await fs.stat(folderPath + name)
                if(stats.isDirectory()){
                    faces[name] = []
                    var images
                    try{
                        images = await fs.readdir(folderPath + name)
                    }catch(err){
                        images = []
                    }
                    for (let i = 0; i < images.length; i++) {
                        let image = images[i]
                        faces[name].push(image)
                    }
                }
            }
            return faces
        }catch(err){
            return []
        }
    }
    const deletePath = async (deletionPath) => {
        try{
            await fs.rm(deletionPath,{ recursive: true, force: true })
        }catch(err){
            console.log('deletePath : Delete Error')
            console.log(err)
        }
    }
    const getFaceFolderPrefix = (groupKey, personName) => {
        const isUnknownFace = (personName.indexOf('UNKN_') > -1)
        return (isUnknownFace ? unknownFacesFolder : facesFolder) + groupKey + '/'
    }
    const createFaceFolder = async (groupKey, sanitizedName) => {
        const faceFolderPrefix = getFaceFolderPrefix(groupKey, sanitizedName)
        try{
            await fs.mkdir(faceFolderPrefix + sanitizedName,{recursive: true})
        }catch(err){
            console.log(err)
            return null
        }
        return faceFolderPrefix
    }
    const getFilenamePrefix = (imageFile) => {
        const fileParts = imageFile.split('.')
        delete(fileParts[fileParts.length - 1])
        const fileName = fileParts.filter((value) => {return !!value}).join('.')
        return fileName
    }
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/names', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            const faces = await getFaceFolderNames(groupKey)
            const unknownFaces = await getUnknownFaceFolderNames(groupKey)
            s.closeJsonResponse(res,{
                ok: true,
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/images', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            const faces = await getFaceImages(groupKey)
            const unknownFaces = await getUnknownFaceImages(groupKey)
            s.closeJsonResponse(res,{
                ok: true,
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/image/:name/:image', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            const imagePath = getFaceFolderPrefix(groupKey, req.params.name) + req.params.name + '/' + req.params.image
            try{
                await fs.stat(imagePath)
                res.setHeader('Content-Type', 'image/jpeg')
                createReadStream(imagePath).pipe(res)
            }catch(err){
                console.log(err)
                s.closeJsonResponse(res,{
                    ok: false,
                    msg: lang['File Not Found']
                })
            }
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/image/:name/:image/delete', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            const imagePath = getFaceFolderPrefix(groupKey, req.params.name) + req.params.name + '/' + req.params.image
            await deletePath(imagePath)
            sendDataToConnectedUsers({
                f:'faceManagerImageDeleted',
                faceName: req.params.name,
                fileName: req.params.image,
            }, groupKey)
            const faces = await getFaceFolderNames(groupKey)
            const unknownFaces = await getUnknownFaceFolderNames(groupKey)
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                groupKey: groupKey,
                faces: faces,
                unknownFaces: unknownFaces,
            })
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/delete/:name', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            const facePath = getFaceFolderPrefix(groupKey, req.params.name) + req.params.name
            await deletePath(facePath)
            const faces = await getFaceFolderNames(groupKey)
            const unknownFaces = await getUnknownFaceFolderNames(groupKey)
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                groupKey: groupKey,
                faces: faces,
                unknownFaces: unknownFaces,
            })
            sendDataToConnectedUsers({
                f:'faceManagerFolderDeleted',
                faceName: req.params.name,
            }, groupKey)
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/image/:name/:image/move/:newName/:newImage', function (req,res){
        s.auth(req.params,function(resp){
            const groupKey = req.params.ke
            s.sendToAllDetectors({
                f: 'moveFaceImage',
                groupKey: groupKey,
                personName: req.params.name,
                imageFile: req.params.image,
                newPersonName: req.params.newName,
                newImageFile: req.params.newImage,
            })
            if(req.query.websocketResponse){
                sendDataToConnectedUsers({
                    f:'faceManagerImageDeleted',
                    faceName: req.params.name,
                    fileName: req.params.image,
                }, groupKey)
                var fileLink = `${config.webPaths.apiPrefix}${req.params.auth}/faceManager/${req.params.ke}/image/${req.params.newName}/${req.params.newImage}`
                sendDataToConnectedUsers({
                    f:'faceManagerImageUploaded',
                    faceName: req.params.newName,
                    fileName: req.params.newImage,
                    url: fileLink
                }, groupKey)
            }
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/moveFace/:name/:newName', function (req,res){
        s.auth(req.params,function(resp){
            const groupKey = req.params.ke
            s.sendToAllDetectorsWithCallback({
                f: 'moveFaceFolder',
                groupKey: groupKey,
                personName: req.params.name,
                newPersonName: req.params.newName,
            },(fileList) => {
                fileList.forEach((fileName) => {
                    if(req.query.websocketResponse){
                        sendDataToConnectedUsers({
                            f:'faceManagerImageDeleted',
                            faceName: req.params.name,
                            fileName: fileName,
                        }, groupKey)
                        var fileLink = `${config.webPaths.apiPrefix}${req.params.auth}/faceManager/${req.params.ke}/${req.params.newName}/${fileName}`
                        sendDataToConnectedUsers({
                            f:'faceManagerImageUploaded',
                            faceName: req.params.newName,
                            fileName: fileName,
                            url: fileLink
                        }, groupKey)
                    }
                })
            })
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.apiPrefix+':auth/faceManager/:ke/create/:name', function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            s.closeJsonResponse(res,{ok: !!await createFaceFolder(groupKey,req.params.name)})
        },res,req)
    })
    app.post(config.webPaths.apiPrefix+':auth/faceManager/:ke/image/:name', fileUpload(), async function (req,res){
        s.auth(req.params, async function(resp){
            const groupKey = req.params.ke
            var fileKeys = Object.keys(req.files || {})
            if(fileKeys.length == 0){
                return res.status(400).send('No files were uploaded.')
            }
            var filesUploaded = []
            var checkFile = async (file) => {
                if(file.name.indexOf('.jpg') > -1 || file.name.indexOf('.jpeg') > -1){
                    const sanitizedName = req.params.name.replace('UNKN_','')
                    filesUploaded.push(file.name)
                    const faceFolderPrefix = await createFaceFolder(groupKey,sanitizedName)
                    file.mv(faceFolderPrefix + sanitizedName + '/' + file.name, function(err) {
                        var fileLink = `${config.webPaths.apiPrefix}${req.params.auth}/faceManager/${req.params.ke}/${sanitizedName}/${file.name}`
                        sendDataToConnectedUsers({
                            f:'faceManagerImageUploaded',
                            faceName: sanitizedName,
                            fileName: file.name,
                            url: fileLink
                        }, groupKey)
                    })
                }
            }
            for (let i = 0; i < fileKeys.length; i++) {
                let key = fileKeys[i]
                var file = req.files[key]
                try{
                    if(file instanceof Array){
                        for (let i = 0; i < file.length; i++) {
                            let fileOfFile = file[i]
                            await checkFile(fileOfFile)
                        }
                    }else{
                        await checkFile(file)
                    }
                }catch(err){
                    console.log(file)
                    console.log(err)
                }
            }
            var response = {
                ok: true,
                filesUploaded: filesUploaded
            }
            s.closeJsonResponse(res,response)
            const faces = await getFaceFolderNames(groupKey)
            const unknownFaces = await getUnknownFaceFolderNames(groupKey)
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                groupKey: groupKey,
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
    const menuItems = s.definitions.SideMenu.blocks.Container1.links
    menuItems.splice(menuItems.findIndex((item) => {
        return item.pageOpen === 'regionEditor'
    }) + 1, 0, {
        icon: 'id-card',
        label: `${lang['Face Manager'] || 'Face Manager'}`,
        pageOpen: 'faceManager',
    })
}
