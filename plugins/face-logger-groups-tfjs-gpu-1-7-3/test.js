console.log('############################################')
console.log('@tensorflow/tfjs-node(-gpu) module test for Facial Recognition (Face Detector)')

// Base Init >>
var fs = require('fs');
const fetch = require('node-fetch');
// Base Init />>
var tf
const tfjsBuild = process.argv[2]
try{
    switch(tfjsBuild){
        case'gpu':
            console.log('GPU Test for Tensorflow Module')
            tf = require('@tensorflow/tfjs-node-gpu')
        break;
        case'cpu':
            console.log('CPU Test for Tensorflow Module')
            tf = require('@tensorflow/tfjs-node')
        break;
        default:
            console.log('Nothing selected, using CPU Module for test.')
            console.log(`Hint : Run the script like one of the following to specify cpu or gpu.`)
            console.log(`node test.js cpu`)
            console.log(`node test.js gpu`)
            tf = require('@tensorflow/tfjs-node')
        break;
    }
}catch(err){
    console.log(`Selection Failed. Could not load desired module. ${tfjsBuild}`)
    console.log(err)
}
if(!tf){
    try{
        tf = require('@tensorflow/tfjs-node-gpu')
    }catch(err){
        try{
            tf = require('@tensorflow/tfjs-node')
        }catch(err){
            return console.log('tfjs-node could not be loaded')
        }
    }
}
console.log('############################################')
const weightLocation = __dirname + '/weights'
const minConfidence = 0.5
const canvas = require('canvas')
const faceapi = require('face-api.js')
const { createCanvas, Image, ImageData, Canvas } = canvas;
faceapi.env.monkeyPatch({ Canvas, Image, ImageData });
var faceDetectionOptions;

async function init() {
    await faceapi.nets.ssdMobilenetv1.loadFromDisk(weightLocation)
    await faceapi.nets.faceLandmark68Net.loadFromDisk(weightLocation)
    await faceapi.nets.faceRecognitionNet.loadFromDisk(weightLocation)
    const faceDetectionNet = faceapi.nets.ssdMobilenetv1
    faceDetectionOptions = new faceapi.SsdMobilenetv1Options({ minConfidence });
}

function runDetection(frameBuffer){
    return new Promise((resolve,reject) => {
        try{
            var startTime = new Date()
            var image = new Image;
            image.onload = async function() {
                faceapi.detectAllFaces(image, faceDetectionOptions)
                .withFaceLandmarks()
                .withFaceDescriptors()
                .then(resolve)
                .catch((err) => {
                    console.log('Fade Detect Error!')
                    console.log(err)
                    resolve([])
                })
            }
            image.src = frameBuffer;
        }catch(err){
            console.log(err)
        }
    })
}

const testImageUrl = `https://cdn.shinobi.video/images/test/car.jpg`
const testImageUrl2 = `https://cdn.shinobi.video/images/test/bear.jpg`
const testImageUrl3 = `https://cdn.shinobi.video/images/test/people.jpg`
const runTest = async (imageUrl) => {
    console.log(`Loading ${imageUrl}`)
    const response = await fetch(imageUrl);
    const frameBuffer = await response.buffer();
    console.log(`Detecting upon ${imageUrl}`)
    const results = await runDetection(frameBuffer)
    if(results[0]){
        var mats = []
        console.log('Detected Faces!')
        results.forEach(function(face){
            console.log('A Face Found!')
            console.log(face.detection)
        })
    }else{
        console.log('No Faces...')
    }
    console.log(`Done ${imageUrl}`)
}
const allTests = async () => {
    await init()
    // await runTest(testImageUrl)
    // await runTest(testImageUrl2)
    await runTest(testImageUrl3)
}
allTests()
